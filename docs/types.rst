.. SPDX-License-Identifier: Apache-2.0
   SPDX-FileCopyrightText: 2020 Collabora, Ltd. and the Ballparker contributors

Ballparker Types
----------------

.. automodule:: ballparker.types

The Task Type
=============

.. autoclass:: Task
   :members:


Constants
=========

.. autoclass:: TShirtSizes
   :members:


.. autodata:: ALL_LEVELS
