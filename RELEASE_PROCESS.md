# Ballparker Release Process

<!--
Copyright 2020, Collabora, Ltd.
SPDX-License-Identifier: CC0-1.0
-->

These are somewhat-terse notes describing the release process, so I don't miss a
step.

## Prep

Ensure all tests are running properly, and that all outstanding and relevant
MRs are merged. You may also want to try the following in a dev venv:

```sh
python3 setup.py sdist bdist_wheel
twine check dist/*
```

## Changelog

Update changelog with [proclamation](https://proclamation.rtfd.io):

```sh
proclamation build -d --overwrite 1.0.x
```

Manually modify as required, then commit the changes and the fragment removal.

## Version string

Update the version string in `ballparker/__init__.py`. (All other places that
care about the version read it from here.)

Reference: <https://www.python.org/dev/peps/pep-0396/>

## Git operations

Make a signed tag, then push the branch and tag to GitLab. (Replace version as appropriate.)

```sh
git checkout main  # and make sure this is what you want to tag!
git tag -s --local-user your.email.here@example.com release-1.0.x -m "Release 1.0.x"
git push origin main
git push origin release-1.0.x
```

## Packaging

Set up (or enter) a development venv:

```sh
python3 -m venv venv
. venv/bin/activate.fish
python3 -m pip install --upgrade -r requirements-dev.txt
```

Remove old dist files:

```sh
rm -rf dist/
```

Then, in the venv, build and push the packages to PyPI:

```sh
python3 setup.py sdist bdist_wheel
twine upload dist/*
```

Reference for some of these steps:
<https://packaging.python.org/tutorials/packaging-projects/>

## Bump dev version

Update `ballparker/__init__.py` to the next version followed by `.dev0` suffix.
(see <https://www.python.org/dev/peps/pep-0440/>)
